<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Input Application</title>
    </head>
    <body>
        <input type="submit" value="Logout" style="float:right" />
    
 		<form action="/input" method="POST">
 			<div>
 				<span>
 				  <p>Hello _user! </p>
 				</span>
	            Enter the type:<br />
	            <input type="text" name="type" /><br />
	            Enter the project: <br />
	            <input type="text" name="project"><br />
	            Enter the year: <br />
	            <input type="text" name="year"><br />            
	            <input type="submit" value="Submit" />
            </div>
            <br />
            <br />
            <br />
            <br />
        </form>
        <span id="history">List of previously visited URLs</span>
        <span id="data">Display page</span>

    </body>
</html>